# Requirements

To run this project you will need a computer with PHP and composer installed.  
You must modify the connection parameters to your database according to your configuration in config/services.yml (user,
password, host)

# Install

To install the project, you just have to run `composer install` to get all the dependencies

# Database

Create your database with the name `fulll`
To install the database, you just need to run `php fleet sync-database`

# Running the tests

After installing the dependencies you can run the tests with this command `php vendor/behat/behat/bin/behat`.
The result should look like this :
![behat.png](behat.png)