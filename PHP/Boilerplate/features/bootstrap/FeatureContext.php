<?php

declare(strict_types=1);

use Behat\Behat\Context\Context;
use Fulll\App\Calculator;
use Fulll\App\Location;
use Fulll\App\Vehicle;

class FeatureContext implements Context
{
    private array $myFleet;
    private array $otherUserFleet;
    private ?Vehicle $vehicle;
    private ?Location $location;
    private ?string $errorMessage;

    public function __construct()
    {
        $this->myFleet = [];
        $this->otherUserFleet = [];
        $this->vehicle = null;
        $this->location = null;
        $this->errorMessage = null;
    }

    /**
     * @When I multiply :a by :b into :var
     */
    public function iMultiply(int $a, int $b, string $var): void
    {
        $calculator = new Calculator();
        $this->$var = $calculator->multiply($a, $b);
    }

    /**
     * @Then :var should be equal to :value
     */
    public function aShouldBeEqualTo(string $var, int $value): void
    {
        if ($value !== $this->$var) {
            throw new \RuntimeException(sprintf('%s is expected to be equal to %s, got %s', $var, $value, $this->$var));
        }
    }

    /**
     * @Given my fleet
     */
    public function myFleet(): void
    {
        $this->myFleet = [];
    }

    /**
     * @Given the fleet of another user
     */
    public function otherUserFleet(): void
    {
        $this->otherUserFleet = [];
    }

    /**
     * @Given a vehicle
     */
    public function aVehicle(): void
    {
        $this->vehicle = new Vehicle();
    }

    /**
     * @When I park my vehicle at this location
     */
    public function parkVehicleAtLocation(): void
    {
        $this->vehicle->park($this->location);
    }

    /**
     * @Given this vehicle has been registered into the other user's fleet
     */
    public function vehicleRegisteredInOtherUserFleet(): void
    {
        $this->otherUserFleet[] = $this->vehicle;
    }

    /**
     * @When I register this vehicle into my fleet
     */
    public function registerVehicleIntoMyFleet(): void
    {
        $this->myFleet[] = $this->vehicle;
    }

    /**
     * @Then this vehicle should be part of my vehicle fleet
     */
    public function vehicleShouldBePartOfMyFleet(): void
    {
        if (!in_array($this->vehicle, $this->myFleet, true)) {
            throw new \RuntimeException("Vehicle is not part of my vehicle fleet");
        }
    }

    /**
     * @When I try to register this vehicle into my fleet
     */
    public function tryToRegisterVehicleIntoMyFleet(): void
    {
        if (in_array($this->vehicle, $this->myFleet, true)) {
            $this->errorMessage = "Vehicle has already been registered into my fleet";
        }
    }

    /**
     * @Given I have registered this vehicle into my fleet
     */
    public function iHaveRegisteredThisVehicleIntoMyFleet(): void
    {
        throw new \RuntimeException("Vehicle registered into my fleet");
    }

    /**
     * @Given a location
     */
    public function aLocation(): void
    {
        $this->location = new Location();
    }

    /**
     * @Then the known location of my vehicle should verify this location
     */
    public function theKnownLocationOfMyVehicleShouldVerifyThisLocation(): void
    {
        if (!$this->vehicle->isParkedAt($this->location)) {
            throw new \RuntimeException("The known location of my vehicle does not match this location");
        }
    }

    /**
     * @Given my vehicle has been parked into this location
     */
    public function myVehicleHasBeenParkedIntoThisLocation(): void
    {
        $this->vehicle->park($this->location);
    }

    /**
     * @When I try to park my vehicle at this location
     */
    public function iTryToParkMyVehicleAtThisLocation(): void
    {
        if ($this->vehicle->isParkedAt($this->location)) {
            $this->errorMessage = "Vehicle is already parked at this location";
        }
    }

    /**
     * @Then I should be informed that my vehicle is already parked at this location
     */
    public function iShouldBeInformedThatMyVehicleIsAlreadyParkedAtThisLocation(): void
    {
        if ($this->errorMessage !== "Vehicle is already parked at this location") {
            throw new \RuntimeException("Incorrect error message received");
        }
    }

    /**
     * @Then I should be informed this vehicle has already been registered into my fleet
     */
    public function iShouldBeInformedThisVehicleHasAlreadyBeenRegisteredIntoMyFleet(): void
    {
        if ($this->errorMessage !== "Vehicle has already been registered into my fleet") {
            throw new \RuntimeException("Incorrect error message received");
        }
    }
}
