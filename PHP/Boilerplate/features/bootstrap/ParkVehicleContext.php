<?php

use Behat\Behat\Context\Context;
use Fulll\Tests\Location;
use Fulll\Tests\Vehicle;

class ParkVehicleContext implements Context
{
    private array $myFleet;
    private ?Vehicle $vehicle;
    private ?Location $location;
    private ?string $errorMessage;

    public function __construct()
    {
        $this->myFleet = [];
        $this->vehicle = null;
        $this->location = null;
        $this->errorMessage = null;
    }

    /**
     * @Given my fleet
     */
    public function myFleet(): void
    {
        $this->myFleet = [];
    }

    /**
     * @Given a vehicle
     */
    public function aVehicle(): void
    {
        $this->vehicle = new Vehicle();
    }

    /**
     * @Given a location
     */
    public function aLocation(): void
    {
        $this->location = new Location();
    }

    /**
     * @When I park my vehicle at this location
     */
    public function parkVehicleAtLocation(): void
    {
        $this->vehicle->park($this->location);
    }

    /**
     * @Then the known location of my vehicle should verify this location
     */
    public function theKnownLocationOfMyVehicleShouldVerifyThisLocation(): void
    {
        if (!$this->vehicle->isParkedAt($this->location)) {
            throw new \RuntimeException("The known location of my vehicle does not match this location");
        }
    }

    /**
     * @Given my vehicle has been parked into this location
     */
    public function myVehicleHasBeenParkedIntoThisLocation(): void
    {
        $this->vehicle->park($this->location);
    }

    /**
     * @When I try to park my vehicle at this location
     */
    public function iTryToParkMyVehicleAtThisLocation(): void
    {
        if ($this->vehicle->isParkedAt($this->location)) {
            $this->errorMessage = "Vehicle is already parked at this location";
        }
    }

    /**
     * @Then I should be informed that my vehicle is already parked at this location
     */
    public function iShouldBeInformedThatMyVehicleIsAlreadyParkedAtThisLocation(): void
    {
        if ($this->errorMessage !== "Vehicle is already parked at this location") {
            throw new \RuntimeException("Incorrect error message received");
        }
    }
}