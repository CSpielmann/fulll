<?php

use Behat\Behat\Context\Context;
use Fulll\Tests\Vehicle;

class RegisterVehicleContext implements Context
{
    private array $myFleet;
    private array $otherUserFleet;
    private ?Vehicle $vehicle;
    private ?string $errorMessage;

    public function __construct()
    {
        $this->myFleet = [];
        $this->otherUserFleet = [];
        $this->vehicle = null;
        $this->errorMessage = null;
    }

    /**
     * @Given my fleet
     */
    public function myFleet(): void
    {
        $this->myFleet = [];
    }

    /**
     * @Given the fleet of another user
     */
    public function otherUserFleet(): void
    {
        $this->otherUserFleet = [];
    }

    /**
     * @Given a vehicle
     */
    public function aVehicle(): void
    {
        $this->vehicle = new Vehicle();
    }

    /**
     * @Given this vehicle has been registered into the other user's fleet
     */
    public function vehicleRegisteredInOtherUserFleet(): void
    {
        $this->otherUserFleet[] = $this->vehicle;
    }

    /**
     * @When I register this vehicle into my fleet
     */
    public function registerVehicleIntoMyFleet(): void
    {
        $this->myFleet[] = $this->vehicle;
    }

    /**
     * @Then this vehicle should be part of my vehicle fleet
     */
    public function vehicleShouldBePartOfMyFleet(): void
    {
        if (!in_array($this->vehicle, $this->myFleet, true)) {
            throw new \RuntimeException("Vehicle is not part of my vehicle fleet");
        }
    }

    /**
     * @When I try to register this vehicle into my fleet
     */
    public function tryToRegisterVehicleIntoMyFleet(): void
    {
        if (in_array($this->vehicle, $this->myFleet, true)) {
            $this->errorMessage = "Vehicle has already been registered into my fleet";
        }
    }

    /**
     * @Given I have registered this vehicle into my fleet
     */
    public function iHaveRegisteredThisVehicleIntoMyFleet(): void
    {
        throw new \RuntimeException("Vehicle registered into my fleet");
    }

    /**
     * @Then I should be informed this vehicle has already been registered into my fleet
     */
    public function iShouldBeInformedThisVehicleHasAlreadyBeenRegisteredIntoMyFleet(): void
    {
        if ($this->errorMessage !== "Vehicle has already been registered into my fleet") {
            throw new \RuntimeException("Incorrect error message received");
        }
    }

}