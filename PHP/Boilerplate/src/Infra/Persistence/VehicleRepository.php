<?php

namespace Fulll\Infra\Persistence;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Fulll\Domain\Model\Vehicle;

class VehicleRepository extends EntityRepository
{
    public function __construct(EntityManagerInterface $manager)
    {
        parent::__construct($manager, $manager->getClassMetadata(Vehicle::class));
    }

    /**
     * @param Vehicle $vehicle
     * @return void
     */
    public function save(Vehicle $vehicle): void
    {
        $this->_em->persist($vehicle);
        $this->_em->flush();
    }
}