<?php

namespace Fulll\Infra\Persistence;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Fulll\Domain\Model\Location;

class LocationRepository extends EntityRepository
{
    public function __construct(EntityManagerInterface $manager)
    {
        parent::__construct($manager, $manager->getClassMetadata(Location::class));
    }

    /**
     * @param Location $location
     * @return void
     */
    public function save(Location $location): void
    {
        $this->_em->persist($location);
        $this->_em->flush();
    }
}