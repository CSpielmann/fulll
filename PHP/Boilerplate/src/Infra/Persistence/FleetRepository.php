<?php

namespace Fulll\Infra\Persistence;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Fulll\Domain\Model\Fleet;

class FleetRepository extends EntityRepository
{
    public function __construct(EntityManagerInterface $manager)
    {
        parent::__construct($manager, $manager->getClassMetadata(Fleet::class));
    }

    /**
     * @param Fleet $fleet
     * @return void
     */
    public function save(Fleet $fleet): void
    {
        $this->_em->persist($fleet);
        $this->_em->flush();
    }
}