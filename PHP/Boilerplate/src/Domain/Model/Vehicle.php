<?php

namespace Fulll\Domain\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Fulll\Infra\Persistence\VehicleRepository")
 * @ORM\Table(name="vehicle",indexes={@ORM\Index(columns={"licensePlate"})})
 */
class Vehicle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $licensePlate;

    /**
     * @ORM\ManyToOne(targetEntity="Fulll\Domain\Model\Fleet", inversedBy="vehicles")
     * @ORM\JoinColumn(nullable=false)
     */
    private Fleet $fleet;

    public function getId(): int
    {
        return $this->id;
    }

    public function getLicensePlate(): string
    {
        return $this->licensePlate;
    }

    public function setLicensePlate(string $licensePlate): self
    {
        $this->licensePlate = $licensePlate;

        return $this;
    }

    public function getFleet(): Fleet
    {
        return $this->fleet;
    }

    public function setFleet(Fleet $fleet): self
    {
        $this->fleet = $fleet;

        return $this;
    }
}