<?php

namespace Fulll\Domain\Services;

use Fulll\Domain\Model\Location;
use Fulll\Domain\Model\Vehicle;
use Fulll\Infra\Persistence\LocationRepository;

class LocationService
{
    private LocationRepository $repository;
    private VehicleService $vehicleService;
    private FleetService $fleetService;

    /**
     * @param LocationRepository $repository
     * @param VehicleService $vehicleService
     * @param FleetService $fleetService
     */
    public function __construct(
        LocationRepository $repository,
        VehicleService     $vehicleService,
        FleetService       $fleetService
    )
    {
        $this->repository = $repository;
        $this->vehicleService = $vehicleService;
        $this->fleetService = $fleetService;
    }

    /**
     * @param int $fleetId
     * @param string $licensePlate
     * @param float $latitude
     * @param float $longitude
     * @param float|null $altitude
     * @return string
     */
    public function parkVehicle(
        int    $fleetId,
        string $licensePlate,
        float  $latitude,
        float  $longitude,
        float  $altitude = null
    ): string
    {
        $fleet = $this->fleetService->getFleet($fleetId);
        if (null === $fleet) {
            throw new \RuntimeException("Fleet not found");
        }

        $vehicle = $this->vehicleService->getVehicle($licensePlate);
        if (null === $vehicle) {
            throw new \RuntimeException("Vehicle not found");
        }

        $location = $this->getLocationVehicle($vehicle, $latitude, $longitude, $altitude);
        if (null !== $location) {
            throw new \RuntimeException("Your vehicle is already parked at this location");
        }

        $location = new Location();
        $location->setVehicle($vehicle);
        $location->setLatitude($latitude);
        $location->setLongitude($longitude);
        $location->setAltitude($altitude);

        $this->repository->save($location);

        return "Successfully park a vehicle";
    }

    /**
     * @param Vehicle $vehicle
     * @param float $latitude
     * @param float $longitude
     * @param float|null $altitude
     * @return Location|null
     */
    private function getLocationVehicle(
        Vehicle $vehicle,
        float   $latitude,
        float   $longitude,
        float   $altitude = null
    ): ?Location
    {
        return $this->repository->findOneBy([
            'vehicle' => $vehicle,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'altitude' => $altitude,
        ]);
    }
}