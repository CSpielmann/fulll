<?php

namespace Fulll\Domain\Services;

use Fulll\Domain\Model\Fleet;
use Fulll\Infra\Persistence\FleetRepository;

class FleetService
{
    private FleetRepository $repository;

    /**
     * @param FleetRepository $repository
     */
    public function __construct(FleetRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $userId
     * @return int
     */
    public function createFleet(int $userId): int
    {
        $fleet = new Fleet();
        $fleet->setUserId($userId);
        $this->repository->save($fleet);

        return $fleet->getId();
    }

    /**
     * @param int $fleetId
     * @return Fleet|null
     */
    public function getFleet(int $fleetId): ?Fleet
    {
        return $this->repository->find($fleetId);
    }
}