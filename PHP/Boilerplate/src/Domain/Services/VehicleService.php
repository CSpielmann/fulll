<?php

namespace Fulll\Domain\Services;

use Fulll\Domain\Model\Vehicle;
use Fulll\Infra\Persistence\VehicleRepository;

class VehicleService
{
    private VehicleRepository $repository;
    private FleetService $fleetService;

    /**
     * @param VehicleRepository $repository
     * @param FleetService $fleetService
     */
    public function __construct(VehicleRepository $repository, FleetService $fleetService)
    {
        $this->repository = $repository;
        $this->fleetService = $fleetService;
    }

    /**
     * @param int $fleetId
     * @param string $licensePlate
     * @return string
     */
    public function register(int $fleetId, string $licensePlate): string
    {
        $fleet = $this->fleetService->getFleet($fleetId);
        if (null === $fleet) {
            throw new \RuntimeException("Fleet not found");
        }

        $vehicleFleet = $this->repository->findOneBy([
            'licensePlate' => $licensePlate,
            'fleet' => $fleetId
        ]);
        if (null !== $vehicleFleet) {
            throw new \RuntimeException("Vehicle already in your fleet : " . $vehicleFleet->getLicensePlate());
        }

        $vehicle = $this->getVehicle($licensePlate);
        if (null !== $vehicle) {
            throw new \RuntimeException("Vehicle already exist : " . $vehicle->getLicensePlate());
        }

        $vehicle = new Vehicle();
        $vehicle->setFleet($fleet);
        $vehicle->setLicensePlate($licensePlate);

        $this->repository->save($vehicle);

        return "Vehicle : " . $vehicle->getId();
    }

    /**
     * @param string $licensePlate
     * @return Vehicle|null
     */
    public function getVehicle(string $licensePlate): ?Vehicle
    {
        return $this->repository->findOneBy([
            'licensePlate' => $licensePlate
        ]);
    }
}