<?php

namespace Fulll\App;

class Location
{
    private ?string $latitude;
    private ?string $longitude;
    private ?string $altitude;

    public function __construct()
    {
        $this->latitude = null;
        $this->longitude = null;
        $this->altitude = null;
    }

    /**
     * @return string
     */
    public function getLatitude(): string
    {
        return $this->latitude;
    }

    /**
     * @return string
     */
    public function getLongitude(): string
    {
        return $this->longitude;
    }

    /**
     * @return string|null
     */
    public function getAltitude(): ?string
    {
        return $this->altitude;
    }
}