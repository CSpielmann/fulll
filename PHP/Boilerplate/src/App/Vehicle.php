<?php

namespace Fulll\App;

class Vehicle
{
    private ?Location $location;

    public function __construct()
    {
        $this->location = null;
    }

    /**
     * @param $location
     * @return bool
     */
    public function isParkedAt($location): bool
    {
        return $this->location === $location;
    }

    /**
     * @param $location
     * @return void
     */
    public function park($location): void
    {
        $this->location = $location;
    }
}