<?php
namespace Fulll\App;

use Symfony\Component\Console\Application as BaseApplication;

class Application extends BaseApplication
{
    /**
     * @param iterable $commands
     */
    public function __construct(iterable $commands = [])
    {
        parent::__construct();
        $this->setName('Fleet');
        $this->setVersion('1.0.0');
        foreach ($commands as $command) {
            $this->add($command);
        }
    }
}