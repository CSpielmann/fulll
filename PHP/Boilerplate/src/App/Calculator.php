<?php

declare(strict_types=1);

namespace Fulll\App;

class Calculator
{
    /**
     * @param int $a
     * @param int $b
     * @return int
     */
    public function multiply(int $a, int $b): int
    {
        return $a * $b;
    }
}
