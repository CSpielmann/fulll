<?php
declare(strict_types=1);

namespace Fulll\App\CommandHandlers;

use Fulll\App\Commands\LocalizeVehicleCommand;
use Fulll\Domain\Services\LocationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;

class LocalizeVehicleHandler
{
    private LocationService $service;

    /**
     * @param LocationService $service
     */
    public function __construct(LocationService $service)
    {
        $this->service = $service;
    }

    /**
     * @param LocalizeVehicleCommand $command
     * @param SymfonyStyle $ui
     * @return int
     */
    public function handle(LocalizeVehicleCommand $command, SymfonyStyle $ui): int
    {
        $vehicle = $this->service->parkVehicle(
            $command->getFleetId(),
            $command->getLicensePlate(),
            $command->getLatitude(),
            $command->getLongitude(),
            $command->getAltitude(),
        );

        $ui->success($vehicle);

        return Command::SUCCESS;
    }
}
