<?php
declare(strict_types=1);

namespace Fulll\App\CommandHandlers;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Fulll\App\Commands\SyncDatabaseCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;

class SyncDatabaseHandler
{
    protected EntityManagerInterface $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param SyncDatabaseCommand $command
     * @param SymfonyStyle $ui
     * @return int
     */
    public function handle(SyncDatabaseCommand $command, SymfonyStyle $ui): int
    {
        $metadatas = $this->em->getMetadataFactory()->getAllMetadata();

        if (empty($metadatas)) {
            $ui->success('No Metadata Classes to process.');

            return 0;
        }

        $schemaTool = new SchemaTool($this->em);

        $sqls = $schemaTool->getUpdateSchemaSql($metadatas, true);
        if (empty($sqls)) {
            $ui->success('Nothing to update - your database is already in sync with the current entity metadata.');

            return 0;
        }

        $ui->newLine();
        $ui->text('Updating database schema...');
        $ui->newLine();

        $schemaTool->updateSchema($metadatas, true);

        $pluralization = (1 === count($sqls)) ? 'query was' : 'queries were';

        $ui->text(sprintf('    <info>%s</info> %s executed', count($sqls), $pluralization));
        $ui->success('Database schema updated successfully!');

        return Command::SUCCESS;
    }
}
