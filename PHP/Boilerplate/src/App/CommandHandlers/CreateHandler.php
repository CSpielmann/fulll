<?php
declare(strict_types=1);

namespace Fulll\App\CommandHandlers;

use Fulll\Domain\Services\FleetService;
use Fulll\App\Commands\CreateCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateHandler
{
    private FleetService $service;

    /**
     * @param FleetService $service
     */
    public function __construct(FleetService $service)
    {
        $this->service = $service;
    }

    /**
     * @param CreateCommand $command
     * @param SymfonyStyle $ui
     * @return int
     */
    public function handle(CreateCommand $command, SymfonyStyle $ui): int
    {
        $fleetId = $this->service->createFleet($command->getUserId());
        $ui->success('Fleet id : ' . $fleetId);

        return Command::SUCCESS;
    }
}
