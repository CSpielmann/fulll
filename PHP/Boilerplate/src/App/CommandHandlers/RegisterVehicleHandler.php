<?php
declare(strict_types=1);

namespace Fulll\App\CommandHandlers;

use Fulll\App\Commands\RegisterVehicleCommand;
use Fulll\Domain\Services\VehicleService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;

class RegisterVehicleHandler
{
    private VehicleService $service;

    /**
     * @param VehicleService $service
     */
    public function __construct(VehicleService $service)
    {
        $this->service = $service;
    }

    /**
     * @param RegisterVehicleCommand $command
     * @param SymfonyStyle $ui
     * @return int
     */
    public function handle(RegisterVehicleCommand $command, SymfonyStyle $ui): int
    {
        $vehicle = $this->service->register($command->getFleetId(), $command->getLicensePlate());
        $ui->success($vehicle);

        return Command::SUCCESS;
    }
}
