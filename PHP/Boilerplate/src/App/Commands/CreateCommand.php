<?php
declare(strict_types=1);

namespace Fulll\App\Commands;

use Fulll\App\CommandHandlers\CreateHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateCommand extends Command
{
    protected static $defaultName = 'create';
    protected CreateHandler $handler;
    protected int $userId;

    /**
     * @param CreateHandler $handler
     */
    public function __construct(CreateHandler $handler)
    {
        $this->handler = $handler;
        parent::__construct();
    }


    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Créé une flotte et l\'affecte à un utilisateur')
            ->addArgument('userId', InputArgument::REQUIRED, 'ID Utilisateur');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->userId = (int)$input->getArgument('userId');

        return $this->handler->handle($this, new SymfonyStyle($input, $output));
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
}
