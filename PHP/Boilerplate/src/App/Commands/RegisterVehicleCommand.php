<?php
declare(strict_types=1);

namespace Fulll\App\Commands;

use Fulll\App\CommandHandlers\RegisterVehicleHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RegisterVehicleCommand extends Command
{
    protected static $defaultName = 'register-vehicle';
    private RegisterVehicleHandler $handler;
    protected int $fleetId;
    protected string $licensePlate;

    /**
     * @param RegisterVehicleHandler $handler
     */
    public function __construct(RegisterVehicleHandler $handler)
    {
        $this->handler = $handler;
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Enregistre un véhicule')
            ->addArgument('fleetId', InputArgument::REQUIRED, 'ID de la flotte')
            ->addArgument('vehiclePlateNumber', InputArgument::REQUIRED, 'Plaque d\'immatriculation du véhicule');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->fleetId = (int)$input->getArgument('fleetId');
        $this->licensePlate = $input->getArgument('vehiclePlateNumber');

        return $this->handler->handle($this, new SymfonyStyle($input, $output));
    }

    /**
     * @return int
     */
    public function getFleetId(): int
    {
        return $this->fleetId;
    }

    /**
     * @return string
     */
    public function getLicensePlate(): string
    {
        return $this->licensePlate;
    }
}
