<?php
declare(strict_types=1);

namespace Fulll\App\Commands;

use Fulll\App\CommandHandlers\SyncDatabaseHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SyncDatabaseCommand extends Command
{
    protected static $defaultName = 'sync-database';
    protected SyncDatabaseHandler $handler;

    /**
     * @param SyncDatabaseHandler $handler
     */
    public function __construct(SyncDatabaseHandler $handler)
    {
        $this->handler = $handler;
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Executes the SQL needed to update the database schema to match the current mapping metadata');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        return $this->handler->handle($this, new SymfonyStyle($input, $output));
    }
}
