<?php
declare(strict_types=1);

namespace Fulll\App\Commands;

use Fulll\App\CommandHandlers\LocalizeVehicleHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class LocalizeVehicleCommand extends Command
{
    protected static $defaultName = 'localize-vehicle';
    private LocalizeVehicleHandler $handler;
    protected int $fleetId;
    protected string $licensePlate;
    protected float $latitude;
    protected float $longitude;
    protected ?float $altitude;

    /**
     * @param LocalizeVehicleHandler $handler
     */
    public function __construct(LocalizeVehicleHandler $handler)
    {
        $this->handler = $handler;
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Localiser un Véhicule')
            ->addArgument('fleetId', InputArgument::REQUIRED, 'ID de la flotte')
            ->addArgument('vehiclePlateNumber', InputArgument::REQUIRED, 'Plaque d\'immatriculation du véhicule')
            ->addArgument('latitude', InputArgument::REQUIRED, 'Latitude de la localisation')
            ->addArgument('longitude', InputArgument::REQUIRED, 'Longitude de la localisation')
            ->addArgument('altitude', InputArgument::OPTIONAL, 'Altitude de la localisation');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->fleetId = (int)$input->getArgument('fleetId');
        $this->licensePlate = $input->getArgument('vehiclePlateNumber');
        $this->latitude = (float)$input->getArgument('latitude');
        $this->longitude = (float)$input->getArgument('longitude');
        $this->altitude = (float)$input->getArgument('altitude');

        return $this->handler->handle($this, new SymfonyStyle($input, $output));
    }

    /**
     * @return int
     */
    public function getFleetId(): int
    {
        return $this->fleetId;
    }

    /**
     * @return string
     */
    public function getLicensePlate(): string
    {
        return $this->licensePlate;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @return float|null
     */
    public function getAltitude(): ?float
    {
        return $this->altitude;
    }
}
