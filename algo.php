<?php

/**
 * @param int $n
 * @return void
 */
function fizzBuzz(int $n): void
{
    for ($index = 0; $index <= $n; $index++) {
        $output = '';

        if (0 === $index % 3) {
            $output .= 'Fizz';
        }
        if (0 === $index % 5) {
            $output .= 'Buzz';
        }

        echo '' !== $output ? $output : $index;
        echo '<br>';
    }
}

fizzBuzz(50);